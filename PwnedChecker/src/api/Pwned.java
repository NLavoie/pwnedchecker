package api;

import java.util.List;

import utils.Utils;

public class Pwned {
	// https://haveibeenpwned.com/API/v2
	public static int wasPasswordPwned(String password) {
		String digest = Utils.digest(password).toUpperCase();
		String digestStart = digest.substring(0, 5);
		List<String> results = Utils.httpRequest("https://api.pwnedpasswords.com/range/" + digestStart);
		for (int i = 0; i < results.size(); i++) {
			String result = results.get(i);
			int separator = result.indexOf(':');
			int times = Integer.parseInt(result.substring(separator + 1));
			String hash = digestStart + result.substring(0, separator);
			if (hash.equals(digest))
				return times;
		}
		return 0;
	}

	public static String getAccountsPwned(String account) {
		String result = null;
		List<String> results = Utils
				.httpRequest("https://haveibeenpwned.com/api/v2/breachedaccount/" + account);
		return result;
	}
}
