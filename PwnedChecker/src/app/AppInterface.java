package app;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import api.Pwned;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class AppInterface extends JFrame {

	private JPanel contentPane;
	private JTextField textInput;
	private JTextArea textResults;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppInterface frame = new AppInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AppInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitle = new JLabel("Breach Checker");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitle.setBounds(10, 11, 414, 40);
		contentPane.add(lblTitle);
		
		textInput = new JTextField();
		textInput.setBounds(76, 74, 273, 20);
		contentPane.add(textInput);
		textInput.setColumns(10);
		
		JButton btnPassword = new JButton("Check password");
		btnPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int wasPwned = Pwned.wasPasswordPwned(textInput.getText());
				textResults.setText("Your password was " + (wasPwned > 0 ? ("found " + wasPwned + " times") : "not found"));
			}
		});
		btnPassword.setBounds(145, 105, 130, 23);
		contentPane.add(btnPassword);
		
		textResults = new JTextArea();
		textResults.setBounds(76, 139, 273, 111);
		contentPane.add(textResults);
	}
}
