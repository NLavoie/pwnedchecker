package app;

import java.util.Scanner;

import api.Pwned;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("Enter your password(**Never sent or saved)");
			int wasPwned = Pwned.wasPasswordPwned(sc.nextLine());
			System.out.println("Your password was " + (wasPwned > 0 ? ("found " + wasPwned + " times") : "not found"));
		}
	}

}
